#-------------------------------------------------
#
# Project created by QtCreator 2017-06-09T12:24:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PCTree
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ArrayFuncs.cpp

HEADERS  += mainwindow.h \
    ArrayFuncs.h

FORMS    += mainwindow.ui
